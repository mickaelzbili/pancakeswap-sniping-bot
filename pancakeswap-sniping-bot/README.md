# TUTORIAL

Hey,

If you want to snipe and swap tokens for fun and profits check out my bot that I built on BSC. It can snipe newly added tokens and swap tokens using Pancakeswap (The one and only) .


# HOW TO RUN

Okay so to run the bot you just need Visual Studio Code, nodejs installed and that's it:

0. https://nodejs.org/en/download/ - Install nodejs
0. Open the folder "pancakeswap-sniping-bot" you downloaded in Visual Studio Code and click on the "Terminal" tab
1. "npm install" write this into the terminal and it will install all modules (libraries)
2. Set the settings in "Bot settings" at the top of trading.js
3. Input enough funds for fees and purchases into your wallet (BNB, WBNB)
4. Run with "node trading.js" command in the same terminal
5. Stop bot with Ctrl + C.

If an error appears:

1. Your gas price is too small 
1. Your slippage is too small (use 20+ for early token)
